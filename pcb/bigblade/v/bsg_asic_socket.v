// this is the ASIC FPGA "socket"

module bsg_asic_socket

  (
  // misc
   input  [6:0] ML0_I
  ,input  [2:0] ML1_I
  ,input  [2:0] MR0_I
  ,input  [7:0] MR1_I

  // bsg_link
  ,input  [15:0] MEM_CLK_I, MEM_V_I, MEM_TKN_I, MEM_EXTRA_I
  ,output [15:0] MEM_CLK_O, MEM_V_O, MEM_TKN_O, MEM_EXTRA_O
  ,input  [15:0][15:0] MEM_DATA_I
  ,output [15:0][15:0] MEM_DATA_O

  ,input  [1:0] IO_CLK_I, IO_V_I, IO_TKN_I, IO_EXTRA_I
  ,output [1:0] IO_CLK_O, IO_V_O, IO_TKN_O, IO_EXTRA_O
  ,input  [1:0][15:0] IO_DATA_I
  ,output [1:0][15:0] IO_DATA_O

  ,input  [1:0] CTRL_CLK_I, CTRL_V_I, CTRL_TKN_I
  ,output [1:0] CTRL_CLK_O, CTRL_V_O, CTRL_TKN_O
  ,input  [1:0][7:0] CTRL_DATA_I
  ,output [1:0][7:0] CTRL_DATA_O
  );

`define BSG_ASIC_SOCKET_16B_CHANNEL_PORTS(pad, typ, i) \
     .p_pad_``pad``_clk_i  (``typ``_CLK_I [i]    )     \
    ,.p_pad_``pad``_v_i    (``typ``_V_I   [i]    )     \
    ,.p_pad_``pad``_tkn_o  (``typ``_TKN_O [i]    )     \
    ,.p_pad_``pad``_0_i    (``typ``_DATA_I[i][ 0])     \
    ,.p_pad_``pad``_1_i    (``typ``_DATA_I[i][ 1])     \
    ,.p_pad_``pad``_2_i    (``typ``_DATA_I[i][ 2])     \
    ,.p_pad_``pad``_3_i    (``typ``_DATA_I[i][ 3])     \
    ,.p_pad_``pad``_4_i    (``typ``_DATA_I[i][ 4])     \
    ,.p_pad_``pad``_5_i    (``typ``_DATA_I[i][ 5])     \
    ,.p_pad_``pad``_6_i    (``typ``_DATA_I[i][ 6])     \
    ,.p_pad_``pad``_7_i    (``typ``_DATA_I[i][ 7])     \
    ,.p_pad_``pad``_8_i    (``typ``_DATA_I[i][ 8])     \
    ,.p_pad_``pad``_9_i    (``typ``_DATA_I[i][ 9])     \
    ,.p_pad_``pad``_10_i   (``typ``_DATA_I[i][10])     \
    ,.p_pad_``pad``_11_i   (``typ``_DATA_I[i][11])     \
    ,.p_pad_``pad``_12_i   (``typ``_DATA_I[i][12])     \
    ,.p_pad_``pad``_13_i   (``typ``_DATA_I[i][13])     \
    ,.p_pad_``pad``_14_i   (``typ``_DATA_I[i][14])     \
    ,.p_pad_``pad``_15_i   (``typ``_DATA_I[i][15])     \
                                                       \
    ,.p_pad_``pad``_clk_o  (``typ``_CLK_O [i]    )     \
    ,.p_pad_``pad``_v_o    (``typ``_V_O   [i]    )     \
    ,.p_pad_``pad``_tkn_i  (``typ``_TKN_I [i]    )     \
    ,.p_pad_``pad``_0_o    (``typ``_DATA_O[i][ 0])     \
    ,.p_pad_``pad``_1_o    (``typ``_DATA_O[i][ 1])     \
    ,.p_pad_``pad``_2_o    (``typ``_DATA_O[i][ 2])     \
    ,.p_pad_``pad``_3_o    (``typ``_DATA_O[i][ 3])     \
    ,.p_pad_``pad``_4_o    (``typ``_DATA_O[i][ 4])     \
    ,.p_pad_``pad``_5_o    (``typ``_DATA_O[i][ 5])     \
    ,.p_pad_``pad``_6_o    (``typ``_DATA_O[i][ 6])     \
    ,.p_pad_``pad``_7_o    (``typ``_DATA_O[i][ 7])     \
    ,.p_pad_``pad``_8_o    (``typ``_DATA_O[i][ 8])     \
    ,.p_pad_``pad``_9_o    (``typ``_DATA_O[i][ 9])     \
    ,.p_pad_``pad``_10_o   (``typ``_DATA_O[i][10])     \
    ,.p_pad_``pad``_11_o   (``typ``_DATA_O[i][11])     \
    ,.p_pad_``pad``_12_o   (``typ``_DATA_O[i][12])     \
    ,.p_pad_``pad``_13_o   (``typ``_DATA_O[i][13])     \
    ,.p_pad_``pad``_14_o   (``typ``_DATA_O[i][14])     \
    ,.p_pad_``pad``_15_o   (``typ``_DATA_O[i][15])     \
                                                       \
    ,.p_pad_``pad``_extra_i(``typ``_EXTRA_I[i])        \
    ,.p_pad_``pad``_extra_o(``typ``_EXTRA_O[i])

`define BSG_ASIC_SOCKET_8B_CHANNEL_PORTS(pad, typ, i) \
     .p_pad_``pad``_clk_i (``typ``_CLK_I [i]    )     \
    ,.p_pad_``pad``_v_i   (``typ``_V_I   [i]    )     \
    ,.p_pad_``pad``_tkn_o (``typ``_TKN_O [i]    )     \
    ,.p_pad_``pad``_0_i   (``typ``_DATA_I[i][ 0])     \
    ,.p_pad_``pad``_1_i   (``typ``_DATA_I[i][ 1])     \
    ,.p_pad_``pad``_2_i   (``typ``_DATA_I[i][ 2])     \
    ,.p_pad_``pad``_3_i   (``typ``_DATA_I[i][ 3])     \
    ,.p_pad_``pad``_4_i   (``typ``_DATA_I[i][ 4])     \
    ,.p_pad_``pad``_5_i   (``typ``_DATA_I[i][ 5])     \
    ,.p_pad_``pad``_6_i   (``typ``_DATA_I[i][ 6])     \
    ,.p_pad_``pad``_7_i   (``typ``_DATA_I[i][ 7])     \
                                                      \
    ,.p_pad_``pad``_clk_o (``typ``_CLK_O [i]    )     \
    ,.p_pad_``pad``_v_o   (``typ``_V_O   [i]    )     \
    ,.p_pad_``pad``_tkn_i (``typ``_TKN_I [i]    )     \
    ,.p_pad_``pad``_0_o   (``typ``_DATA_O[i][ 0])     \
    ,.p_pad_``pad``_1_o   (``typ``_DATA_O[i][ 1])     \
    ,.p_pad_``pad``_2_o   (``typ``_DATA_O[i][ 2])     \
    ,.p_pad_``pad``_3_o   (``typ``_DATA_O[i][ 3])     \
    ,.p_pad_``pad``_4_o   (``typ``_DATA_O[i][ 4])     \
    ,.p_pad_``pad``_5_o   (``typ``_DATA_O[i][ 5])     \
    ,.p_pad_``pad``_6_o   (``typ``_DATA_O[i][ 6])     \
    ,.p_pad_``pad``_7_o   (``typ``_DATA_O[i][ 7])

  bsg_chip ASIC
  (
  // DRAM L CHANNELS
   `BSG_ASIC_SOCKET_16B_CHANNEL_PORTS(DL0, MEM,  0)
  ,`BSG_ASIC_SOCKET_16B_CHANNEL_PORTS(DL1, MEM,  1)
  ,`BSG_ASIC_SOCKET_16B_CHANNEL_PORTS(DL2, MEM,  2)
  ,`BSG_ASIC_SOCKET_16B_CHANNEL_PORTS(DL3, MEM,  3)
  ,`BSG_ASIC_SOCKET_16B_CHANNEL_PORTS(DL4, MEM,  4)
  ,`BSG_ASIC_SOCKET_16B_CHANNEL_PORTS(DL5, MEM,  5)
  ,`BSG_ASIC_SOCKET_16B_CHANNEL_PORTS(DL6, MEM,  6)
  ,`BSG_ASIC_SOCKET_16B_CHANNEL_PORTS(DL7, MEM,  7)

  // DRAM R CHANNELS
  ,`BSG_ASIC_SOCKET_16B_CHANNEL_PORTS(DR0, MEM,  8)
  ,`BSG_ASIC_SOCKET_16B_CHANNEL_PORTS(DR1, MEM,  9)
  ,`BSG_ASIC_SOCKET_16B_CHANNEL_PORTS(DR2, MEM, 10)
  ,`BSG_ASIC_SOCKET_16B_CHANNEL_PORTS(DR3, MEM, 11)
  ,`BSG_ASIC_SOCKET_16B_CHANNEL_PORTS(DR4, MEM, 12)
  ,`BSG_ASIC_SOCKET_16B_CHANNEL_PORTS(DR5, MEM, 13)
  ,`BSG_ASIC_SOCKET_16B_CHANNEL_PORTS(DR6, MEM, 14)
  ,`BSG_ASIC_SOCKET_16B_CHANNEL_PORTS(DR7, MEM, 15)

  // IO T CHANNELS
  ,`BSG_ASIC_SOCKET_16B_CHANNEL_PORTS(IT0, IO, 0)
  ,`BSG_ASIC_SOCKET_16B_CHANNEL_PORTS(IT1, IO, 1)

  // CTRL T/B CHANNELS
  ,`BSG_ASIC_SOCKET_8B_CHANNEL_PORTS(CT0, CTRL, 0)
  ,`BSG_ASIC_SOCKET_8B_CHANNEL_PORTS(CB0, CTRL, 1)

  // MISC L0
  ,.p_pad_ML0_0_i(ML0_I[0])
  ,.p_pad_ML0_1_i(ML0_I[1])
  ,.p_pad_ML0_2_i(ML0_I[2])
  ,.p_pad_ML0_3_i(ML0_I[3])
  ,.p_pad_ML0_4_i(ML0_I[4])
  ,.p_pad_ML0_5_i(ML0_I[5])
  ,.p_pad_ML0_6_i(ML0_I[6])

  // MISC L1
  ,.p_pad_ML1_0_i(ML1_I[0])
  ,.p_pad_ML1_1_i(ML1_I[1])
  ,.p_pad_ML1_2_i(ML1_I[2])

  // MISC R0
  ,.p_pad_MR0_0_i(MR0_I[0])
  ,.p_pad_MR0_1_i(MR0_I[1])
  ,.p_pad_MR0_2_i(MR0_I[2])

  // MISC R1
  ,.p_pad_MR1_0_i(MR1_I[0])
  ,.p_pad_MR1_1_i(MR1_I[1])
  ,.p_pad_MR1_2_i(MR1_I[2])
  ,.p_pad_MR1_3_i(MR1_I[3])
  ,.p_pad_MR1_4_i(MR1_I[4])
  ,.p_pad_MR1_5_i(MR1_I[5])
  ,.p_pad_MR1_6_i(MR1_I[6])
  ,.p_pad_MR1_7_i(MR1_I[7])
  );

endmodule
