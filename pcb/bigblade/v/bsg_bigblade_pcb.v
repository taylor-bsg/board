// bsg_bigblade_pcb.v
//
// simulates connectivity of the PCB
//
// this is intended as the canonical simulation file
// but currently it may only implement a subset of
// all of the wires and functionality -- please
// extend rather than cloning the file and modifying it
//

`timescale 1ps/1ps

module bsg_bigblade_pcb
(
    // this is the FMC connector
    inout [33:00] LAxx_N
    , inout [33:00] LAxx_P      //  e.g. LA14_N
    , inout CLK0_C2M_N
    , inout CLK0_C2M_P
    , inout CLK0_M2C_N
    , inout CLK0_M2C_P

    // SMA connectors (for simulation)
    , input ASIC_SMA_IN_N
    , input ASIC_SMA_IN_P       // terminated on ASIC side
    , inout ASIC_SMA_OUT_N
    , inout ASIC_SMA_OUT_P      // unterminated

    , inout FPGA_SMA_IN_N
    , inout FPGA_SMA_IN_P       // unterminated
    , inout FPGA_SMA_OUT_N
    , inout FPGA_SMA_OUT_P      // unterminated

    // LEDs (for simulation)
    , output [3:0] FPGA_LED     // from GW   FPGA
    , output [1:0] ASIC_LED     // from ASIC FPGA

    , input  UART_RX
    , output UART_TX

    // low-true reset signal for GW FPGA (normal driven by reset controller)
    , input PWR_RSTN
  );

  // bsg link wires (Between GW and IC)
  wire [15:0] IC_GW_MEM_CLK, IC_GW_MEM_V, IC_GW_MEM_TKN, IC_GW_MEM_EXTRA;
  wire [15:0] GW_IC_MEM_CLK, GW_IC_MEM_V, GW_IC_MEM_TKN, GW_IC_MEM_EXTRA;
  wire [15:0][15:0] IC_GW_MEM_DATA, GW_IC_MEM_DATA;

  wire [1:0] IC_GW_IO_CLK, IC_GW_IO_V, IC_GW_IO_TKN, IC_GW_IO_EXTRA;
  wire [1:0] GW_IC_IO_CLK, GW_IC_IO_V, GW_IC_IO_TKN, GW_IC_IO_EXTRA;
  wire [1:0][15:0] IC_GW_IO_DATA, GW_IC_IO_DATA;

  wire [1:0] IC_GW_CTRL_CLK, IC_GW_CTRL_V, IC_GW_CTRL_TKN;
  wire [1:0] GW_IC_CTRL_CLK, GW_IC_CTRL_V, GW_IC_CTRL_TKN;
  wire [1:0][7:0] IC_GW_CTRL_DATA, GW_IC_CTRL_DATA;

  //
  // Misc wires
  //
  wire [6:0] MISC_ML0;
  wire [2:0] MISC_ML1;
  wire [2:0] MISC_MR0;
  wire [7:0] MISC_MR1;

  //
  // Delay top data bits of IO->GW links
  // Duplicate IC->GW CLK signal to EXTRA port
  //
  wire [15:0][15:0] IC_GW_MEM_DATA_DELAY, IC_GW_MEM_DATA_FINAL;
  wire [1:0][15:0] IC_GW_IO_DATA_DELAY, IC_GW_IO_DATA_FINAL;

  localparam wire_dly_lp = 13333;

  bsg_nonsynth_delay_line #(.width_p(16*1),.delay_p(wire_dly_lp)) mem_clk_dly
  (.i(IC_GW_MEM_CLK),.o(IC_GW_MEM_EXTRA));
  bsg_nonsynth_delay_line #(.width_p(16*16),.delay_p(wire_dly_lp)) mem_data_dly
  (.i(IC_GW_MEM_DATA),.o(IC_GW_MEM_DATA_DELAY));
  bsg_nonsynth_delay_line #(.width_p(2*1),.delay_p(wire_dly_lp)) io_clk_dly
  (.i(IC_GW_IO_CLK),.o(IC_GW_IO_EXTRA));
  bsg_nonsynth_delay_line #(.width_p(2*16),.delay_p(wire_dly_lp)) io_data_dly
  (.i(IC_GW_IO_DATA),.o(IC_GW_IO_DATA_DELAY));

  for (genvar i = 0; i < 16; i++)
    assign IC_GW_MEM_DATA_FINAL[i] = {IC_GW_MEM_DATA[i][15:8], IC_GW_MEM_DATA_DELAY[i][7:0]};
  for (genvar i = 0; i < 2; i++)
    assign IC_GW_IO_DATA_FINAL[i] = {IC_GW_IO_DATA[i][15:8], IC_GW_IO_DATA_DELAY[i][7:0]};

  //
  // GATEWAY SOCKET
  //

  bsg_gateway_socket GW
  (.MEM_CLK_I        (IC_GW_MEM_CLK)
  ,.MEM_V_I          (IC_GW_MEM_V)
  ,.MEM_TKN_O        (IC_GW_MEM_TKN)
  ,.MEM_DATA_I       (IC_GW_MEM_DATA_FINAL)
  ,.MEM_EXTRA_I      (IC_GW_MEM_EXTRA)
  ,.MEM_CLK_O        (GW_IC_MEM_CLK)
  ,.MEM_V_O          (GW_IC_MEM_V)
  ,.MEM_TKN_I        (GW_IC_MEM_TKN)
  ,.MEM_DATA_O       (GW_IC_MEM_DATA)
  ,.MEM_EXTRA_O      (GW_IC_MEM_EXTRA)

  ,.IO_CLK_I         (IC_GW_IO_CLK)
  ,.IO_V_I           (IC_GW_IO_V)
  ,.IO_TKN_O         (IC_GW_IO_TKN)
  ,.IO_DATA_I        (IC_GW_IO_DATA_FINAL)
  ,.IO_EXTRA_I       (IC_GW_IO_EXTRA)
  ,.IO_CLK_O         (GW_IC_IO_CLK)
  ,.IO_V_O           (GW_IC_IO_V)
  ,.IO_TKN_I         (GW_IC_IO_TKN)
  ,.IO_DATA_O        (GW_IC_IO_DATA)
  ,.IO_EXTRA_O       (GW_IC_IO_EXTRA)

  ,.CTRL_CLK_I       (IC_GW_CTRL_CLK)
  ,.CTRL_V_I         (IC_GW_CTRL_V)
  ,.CTRL_TKN_O       (IC_GW_CTRL_TKN)
  ,.CTRL_DATA_I      (IC_GW_CTRL_DATA)
  ,.CTRL_CLK_O       (GW_IC_CTRL_CLK)
  ,.CTRL_V_O         (GW_IC_CTRL_V)
  ,.CTRL_TKN_I       (GW_IC_CTRL_TKN)
  ,.CTRL_DATA_O      (GW_IC_CTRL_DATA)

  ,.ML0_O            (MISC_ML0)
  ,.ML1_O            (MISC_ML1)
  ,.MR0_O            (MISC_MR0)
  ,.MR1_O            (MISC_MR1)
  );

  //
  // ASIC SOCKET
  //

  bsg_asic_socket IC
  (.MEM_CLK_I        (GW_IC_MEM_CLK)
  ,.MEM_V_I          (GW_IC_MEM_V)
  ,.MEM_TKN_O        (GW_IC_MEM_TKN)
  ,.MEM_DATA_I       (GW_IC_MEM_DATA)
  ,.MEM_EXTRA_I      (GW_IC_MEM_EXTRA)
  ,.MEM_CLK_O        (IC_GW_MEM_CLK)
  ,.MEM_V_O          (IC_GW_MEM_V)
  ,.MEM_TKN_I        (IC_GW_MEM_TKN)
  ,.MEM_DATA_O       (IC_GW_MEM_DATA)
  ,.MEM_EXTRA_O      () // used for clock duplication

  ,.IO_CLK_I         (GW_IC_IO_CLK)
  ,.IO_V_I           (GW_IC_IO_V)
  ,.IO_TKN_O         (GW_IC_IO_TKN)
  ,.IO_DATA_I        (GW_IC_IO_DATA)
  ,.IO_EXTRA_I       (GW_IC_IO_EXTRA)
  ,.IO_CLK_O         (IC_GW_IO_CLK)
  ,.IO_V_O           (IC_GW_IO_V)
  ,.IO_TKN_I         (IC_GW_IO_TKN)
  ,.IO_DATA_O        (IC_GW_IO_DATA)
  ,.IO_EXTRA_O       () // used for clock duplication

  ,.CTRL_CLK_I       (GW_IC_CTRL_CLK)
  ,.CTRL_V_I         (GW_IC_CTRL_V)
  ,.CTRL_TKN_O       (GW_IC_CTRL_TKN)
  ,.CTRL_DATA_I      (GW_IC_CTRL_DATA)
  ,.CTRL_CLK_O       (IC_GW_CTRL_CLK)
  ,.CTRL_V_O         (IC_GW_CTRL_V)
  ,.CTRL_TKN_I       (IC_GW_CTRL_TKN)
  ,.CTRL_DATA_O      (IC_GW_CTRL_DATA)

  ,.ML0_I            (MISC_ML0)
  ,.ML1_I            (MISC_ML1)
  ,.MR0_I            (MISC_MR0)
  ,.MR1_I            (MISC_MR1)
  );

endmodule
