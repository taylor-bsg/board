// this is the ASIC FPGA "socket"

// FIXME (for paul): update inputs/outputs to match ASIC Cloud PCB names
//                   & update this doc with PCB trace names: https://docs.google.com/spreadsheets/d/1-TwZZIBsyTsBQQUj520goB-E5-BECZgs45nWkxyyeiU/edit#gid=1657494264
// Paul: FIXME FIXED

module bsg_asic_socket
  
  (
  // reset
   input  CLK_RESET_I, CORE_RESET_I

  // clk
  ,input  CLKA_I, CLKB_I, CLKC_I
  ,input  SEL0_I, SEL1_I, SEL2_I
  ,output CLK_O, MISC_O

  // tag
  ,input  TAG_CLK_I, TAG_EN_I, TAG_DATA_I
  ,output TAG_CLK_O, TAG_DATA_O

  // comm-link channel-ci
  // DIRECTION NOT SWITCHED
  // COMING IN from NEXT chip in chain
  // TRACE SEQUENCE NOT REMAPPED
  ,input  CL_IN_CLK_I, CL_IN_V_I
  ,output CL_IN_TKN_O
  ,input  CL_IN_D0_I, CL_IN_D1_I
         ,CL_IN_D2_I, CL_IN_D3_I
         ,CL_IN_D4_I, CL_IN_D5_I
         ,CL_IN_D6_I, CL_IN_D7_I
         ,CL_IN_D8_I

  // comm-link channel-co
  // DIRECTION SWITCHED from OUTPUT to INPUT
  // COMING IN from PREVIOUS chip in chain
  // TRACE SEQUENCE NOT REMAPPED
  ,input  CL_OUT_CLK_I, CL_OUT_V_I
  ,output CL_OUT_TKN_O
  ,input  CL_OUT_D0_I, CL_OUT_D1_I
         ,CL_OUT_D2_I, CL_OUT_D3_I
         ,CL_OUT_D4_I, CL_OUT_D5_I
         ,CL_OUT_D6_I, CL_OUT_D7_I
         ,CL_OUT_D8_I

  // comm-link channel-ci2
  // DIRECTION SWITCHED from INPUT to OUTPUT
  // GOING OUT to NEXT chip in chain
  // TRACE SEQUENCE REMAPPED:
  // port name ---> trace name on PCB
  // CLK ---> CLK
  // V ---> D4
  // TKN ---> TKN
  // D0 ---> D6
  // D1 ---> D5
  // D2 ---> D7
  // D3 ---> D8
  // D4 ---> D3
  // D5 ---> V
  // D6 ---> D2
  // D7 ---> D1
  // D8 ---> D0
  ,output CL2_IN_CLK_O, CL2_IN_V_O
  ,input  CL2_IN_TKN_I
  ,output CL2_IN_D0_O, CL2_IN_D1_O
         ,CL2_IN_D2_O, CL2_IN_D3_O
         ,CL2_IN_D4_O, CL2_IN_D5_O
         ,CL2_IN_D6_O, CL2_IN_D7_O
         ,CL2_IN_D8_O

  // comm-link channel-co2
  // DIRECTION NOT SWITCHED
  // GOING OUT to PREVIOUS chip in chain
  // TRACE SEQUENCE REMAPPED:
  // port name ---> trace name on PCB
  // CLK ---> CLK
  // V ---> D4
  // TKN ---> TKN
  // D0 ---> D8
  // D1 ---> D7
  // D2 ---> V
  // D3 ---> D6
  // D4 ---> D5
  // D5 ---> D3
  // D6 ---> D2
  // D7 ---> D1
  // D8 ---> D0
  ,output CL2_OUT_CLK_O, CL2_OUT_V_O
  ,input  CL2_OUT_TKN_I
  ,output CL2_OUT_D0_O, CL2_OUT_D1_O
         ,CL2_OUT_D2_O, CL2_OUT_D3_O
         ,CL2_OUT_D4_O, CL2_OUT_D5_O
         ,CL2_OUT_D6_O, CL2_OUT_D7_O
         ,CL2_OUT_D8_O

  // DDR commands
  ,output DDR_CK_P_O, DDR_CK_N_O, DDR_CKE_O
  ,output DDR_CS_N_O, DDR_RAS_N_O
         ,DDR_CAS_N_O, DDR_WE_N_O
  ,output DDR_RESET_N_O, DDR_ODT_O
  ,output DDR_BA0_O, DDR_BA1_O, DDR_BA2_O
  ,output DDR_ADDR0_O, DDR_ADDR1_O
         ,DDR_ADDR2_O, DDR_ADDR3_O
         ,DDR_ADDR4_O, DDR_ADDR5_O
         ,DDR_ADDR6_O, DDR_ADDR7_O
         ,DDR_ADDR8_O, DDR_ADDR9_O
         ,DDR_ADDR10_O, DDR_ADDR11_O
         ,DDR_ADDR12_O, DDR_ADDR13_O
         ,DDR_ADDR14_O, DDR_ADDR15_O

  // DDR DATA_0
  ,output DDR_DM0_O
  ,inout  DDR_DQS0_P_IO, DDR_DQS0_N_IO
  ,inout  DDR_DQ0_IO, DDR_DQ1_IO
         ,DDR_DQ2_IO, DDR_DQ3_IO
         ,DDR_DQ4_IO, DDR_DQ5_IO
         ,DDR_DQ6_IO, DDR_DQ7_IO

  // DDR DATA_1
  ,output DDR_DM1_O
  ,inout  DDR_DQS1_P_IO, DDR_DQS1_N_IO
  ,inout  DDR_DQ8_IO, DDR_DQ9_IO
         ,DDR_DQ10_IO, DDR_DQ11_IO
         ,DDR_DQ12_IO, DDR_DQ13_IO
         ,DDR_DQ14_IO, DDR_DQ15_IO

  // DDR DATA_2
  ,output DDR_DM2_O
  ,inout  DDR_DQS2_P_IO, DDR_DQS2_N_IO
  ,inout  DDR_DQ16_IO, DDR_DQ17_IO
         ,DDR_DQ18_IO, DDR_DQ19_IO
         ,DDR_DQ20_IO, DDR_DQ21_IO
         ,DDR_DQ22_IO, DDR_DQ23_IO

  // DDR DATA_3
  ,output DDR_DM3_O
  ,inout  DDR_DQS3_P_IO, DDR_DQS3_N_IO
  ,inout  DDR_DQ24_IO, DDR_DQ25_IO
         ,DDR_DQ26_IO, DDR_DQ27_IO
         ,DDR_DQ28_IO, DDR_DQ29_IO
         ,DDR_DQ30_IO, DDR_DQ31_IO
 
  );

   // see bsg_packaging/uw_bga/pinouts/bsg_asic_cloud/common/verilog/bsg_pinout.v
   // these names correspond to the UW BGA package names

  bsg_chip ASIC
  (.p_ci_clk_i (CL_IN_CLK_I)
  ,.p_ci_v_i   (CL_IN_V_I)
  ,.p_ci_tkn_o (CL_IN_TKN_O)
  ,.p_ci_0_i   (CL_IN_D0_I)
  ,.p_ci_1_i   (CL_IN_D1_I)
  ,.p_ci_2_i   (CL_IN_D2_I)
  ,.p_ci_3_i   (CL_IN_D3_I)
  ,.p_ci_4_i   (CL_IN_D4_I)
  ,.p_ci_5_i   (CL_IN_D5_I)
  ,.p_ci_6_i   (CL_IN_D6_I)
  ,.p_ci_7_i   (CL_IN_D7_I)
  ,.p_ci_8_i   (CL_IN_D8_I)

  ,.p_co_clk_i (CL_OUT_CLK_I)
  ,.p_co_v_i   (CL_OUT_V_I)
  ,.p_co_tkn_o (CL_OUT_TKN_O)
  ,.p_co_0_i   (CL_OUT_D0_I)
  ,.p_co_1_i   (CL_OUT_D1_I)
  ,.p_co_2_i   (CL_OUT_D2_I)
  ,.p_co_3_i   (CL_OUT_D3_I)
  ,.p_co_4_i   (CL_OUT_D4_I)
  ,.p_co_5_i   (CL_OUT_D5_I)
  ,.p_co_6_i   (CL_OUT_D6_I)
  ,.p_co_7_i   (CL_OUT_D7_I)
  ,.p_co_8_i   (CL_OUT_D8_I)

  ,.p_ci2_clk_o(CL2_IN_CLK_O)
  ,.p_ci2_v_o  (CL2_IN_V_O)
  ,.p_ci2_tkn_i(CL2_IN_TKN_I)
  ,.p_ci2_0_o  (CL2_IN_D0_O)
  ,.p_ci2_1_o  (CL2_IN_D1_O)
  ,.p_ci2_2_o  (CL2_IN_D2_O)
  ,.p_ci2_3_o  (CL2_IN_D3_O)
  ,.p_ci2_4_o  (CL2_IN_D4_O)
  ,.p_ci2_5_o  (CL2_IN_D5_O)
  ,.p_ci2_6_o  (CL2_IN_D6_O)
  ,.p_ci2_7_o  (CL2_IN_D7_O)
  ,.p_ci2_8_o  (CL2_IN_D8_O)

  ,.p_co2_clk_o(CL2_OUT_CLK_O)
  ,.p_co2_v_o  (CL2_OUT_V_O)
  ,.p_co2_tkn_i(CL2_OUT_TKN_I)
  ,.p_co2_0_o  (CL2_OUT_D0_O)
  ,.p_co2_1_o  (CL2_OUT_D1_O)
  ,.p_co2_2_o  (CL2_OUT_D2_O)
  ,.p_co2_3_o  (CL2_OUT_D3_O)
  ,.p_co2_4_o  (CL2_OUT_D4_O)
  ,.p_co2_5_o  (CL2_OUT_D5_O)
  ,.p_co2_6_o  (CL2_OUT_D6_O)
  ,.p_co2_7_o  (CL2_OUT_D7_O)
  ,.p_co2_8_o  (CL2_OUT_D8_O)

  // 32-bit ddr dram interface, 72 pins
  // ddr interface differential output clock pair
  ,.p_ddr_ck_p_o(DDR_CK_P_O)
  ,.p_ddr_ck_n_o(DDR_CK_N_O)

  // ddr interface output clock enable signal
  ,.p_ddr_cke_o(DDR_CKE_O)

  // ddr interface output command signals
  ,.p_ddr_cs_n_o (DDR_CS_N_O)
  ,.p_ddr_ras_n_o(DDR_RAS_N_O)
  ,.p_ddr_cas_n_o(DDR_CAS_N_O)
  ,.p_ddr_we_n_o (DDR_WE_N_O)

  // ddr interface output control signals
  ,.p_ddr_reset_n_o (DDR_RESET_N_O)
  ,.p_ddr_odt_o     (DDR_ODT_O)

  // ddr interface bank address
  ,.p_ddr_ba_0_o(DDR_BA0_O)
  ,.p_ddr_ba_1_o(DDR_BA1_O)
  ,.p_ddr_ba_2_o(DDR_BA2_O)

  // ddr interface address bus
  ,.p_ddr_addr_0_o (DDR_ADDR0_O)
  ,.p_ddr_addr_1_o (DDR_ADDR1_O)
  ,.p_ddr_addr_2_o (DDR_ADDR2_O)
  ,.p_ddr_addr_3_o (DDR_ADDR3_O)
  ,.p_ddr_addr_4_o (DDR_ADDR4_O)
  ,.p_ddr_addr_5_o (DDR_ADDR5_O)
  ,.p_ddr_addr_6_o (DDR_ADDR6_O)
  ,.p_ddr_addr_7_o (DDR_ADDR7_O)
  ,.p_ddr_addr_8_o (DDR_ADDR8_O)
  ,.p_ddr_addr_9_o (DDR_ADDR9_O)
  ,.p_ddr_addr_10_o(DDR_ADDR10_O)
  ,.p_ddr_addr_11_o(DDR_ADDR11_O)
  ,.p_ddr_addr_12_o(DDR_ADDR12_O)
  ,.p_ddr_addr_13_o(DDR_ADDR13_O)
  ,.p_ddr_addr_14_o(DDR_ADDR14_O)
  ,.p_ddr_addr_15_o(DDR_ADDR15_O)

  // 32-bit ddr interface data mask
  ,.p_ddr_dm_0_o(DDR_DM0_O)
  ,.p_ddr_dm_1_o(DDR_DM1_O)
  ,.p_ddr_dm_2_o(DDR_DM2_O)
  ,.p_ddr_dm_3_o(DDR_DM3_O)

  // 32-bit ddr interface dq strobe signals
  ,.p_ddr_dqs_p_0_io(DDR_DQS0_P_IO)
  ,.p_ddr_dqs_n_0_io(DDR_DQS0_N_IO)
  ,.p_ddr_dqs_p_1_io(DDR_DQS1_P_IO)
  ,.p_ddr_dqs_n_1_io(DDR_DQS1_N_IO)
  ,.p_ddr_dqs_p_2_io(DDR_DQS2_P_IO)
  ,.p_ddr_dqs_n_2_io(DDR_DQS2_N_IO)
  ,.p_ddr_dqs_p_3_io(DDR_DQS3_P_IO)
  ,.p_ddr_dqs_n_3_io(DDR_DQS3_N_IO)

  // ddr interface data bus
  ,.p_ddr_dq_0_io (DDR_DQ0_IO)
  ,.p_ddr_dq_1_io (DDR_DQ1_IO)
  ,.p_ddr_dq_2_io (DDR_DQ2_IO)
  ,.p_ddr_dq_3_io (DDR_DQ3_IO)
  ,.p_ddr_dq_4_io (DDR_DQ4_IO)
  ,.p_ddr_dq_5_io (DDR_DQ5_IO)
  ,.p_ddr_dq_6_io (DDR_DQ6_IO)
  ,.p_ddr_dq_7_io (DDR_DQ7_IO)
  ,.p_ddr_dq_8_io (DDR_DQ8_IO)
  ,.p_ddr_dq_9_io (DDR_DQ9_IO)
  ,.p_ddr_dq_10_io(DDR_DQ10_IO)
  ,.p_ddr_dq_11_io(DDR_DQ11_IO)
  ,.p_ddr_dq_12_io(DDR_DQ12_IO)
  ,.p_ddr_dq_13_io(DDR_DQ13_IO)
  ,.p_ddr_dq_14_io(DDR_DQ14_IO)
  ,.p_ddr_dq_15_io(DDR_DQ15_IO)
  ,.p_ddr_dq_16_io(DDR_DQ16_IO)
  ,.p_ddr_dq_17_io(DDR_DQ17_IO)
  ,.p_ddr_dq_18_io(DDR_DQ18_IO)
  ,.p_ddr_dq_19_io(DDR_DQ19_IO)
  ,.p_ddr_dq_20_io(DDR_DQ20_IO)
  ,.p_ddr_dq_21_io(DDR_DQ21_IO)
  ,.p_ddr_dq_22_io(DDR_DQ22_IO)
  ,.p_ddr_dq_23_io(DDR_DQ23_IO)
  ,.p_ddr_dq_24_io(DDR_DQ24_IO)
  ,.p_ddr_dq_25_io(DDR_DQ25_IO)
  ,.p_ddr_dq_26_io(DDR_DQ26_IO)
  ,.p_ddr_dq_27_io(DDR_DQ27_IO)
  ,.p_ddr_dq_28_io(DDR_DQ28_IO)
  ,.p_ddr_dq_29_io(DDR_DQ29_IO)
  ,.p_ddr_dq_30_io(DDR_DQ30_IO)
  ,.p_ddr_dq_31_io(DDR_DQ31_IO)

  // bsg tag interface, 5 pins
  ,.p_bsg_tag_clk_i  (TAG_CLK_I)
  ,.p_bsg_tag_en_i   (TAG_EN_I)
  ,.p_bsg_tag_data_i (TAG_DATA_I)
  ,.p_bsg_tag_clk_o  (TAG_CLK_O)
  ,.p_bsg_tag_data_o (TAG_DATA_O)

  // clock and reset interface, 9 pins
  // clock input signals
  ,.p_clk_A_i(CLKA_I)
  ,.p_clk_B_i(CLKB_I)
  ,.p_clk_C_i(CLKC_I)

  // clock output signal
  ,.p_clk_o  (CLK_O)

  // 3-bit clock selection signals
  ,.p_sel_0_i(SEL0_I)
  ,.p_sel_1_i(SEL1_I)
  ,.p_sel_2_i(SEL2_I)

  // asynchronous reset signals
  ,.p_clk_async_reset_i  (CLK_RESET_I)
  ,.p_core_async_reset_i (CORE_RESET_I)

  // miscellaneous signal, 1 pin
  ,.p_misc_o (MISC_O)
  );


endmodule
