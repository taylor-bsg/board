/**
 *  bsg_gateway_socket.v
 */

module bsg_gateway_socket
(
  input CL_IN_CLK_I
  , input CL_IN_V_I
  , output CL_IN_TKN_O
  , input CL_IN_D0_I
  , input CL_IN_D1_I
  , input CL_IN_D2_I
  , input CL_IN_D3_I
  , input CL_IN_D4_I
  , input CL_IN_D5_I
  , input CL_IN_D6_I
  , input CL_IN_D7_I
  , input CL_IN_D8_I

  , input CL_OUT_CLK_I
  , input CL_OUT_V_I
  , output CL_OUT_TKN_O
  , input CL_OUT_D0_I
  , input CL_OUT_D1_I
  , input CL_OUT_D2_I
  , input CL_OUT_D3_I
  , input CL_OUT_D4_I
  , input CL_OUT_D5_I
  , input CL_OUT_D6_I
  , input CL_OUT_D7_I
  , input CL_OUT_D8_I

  , output CL2_IN_CLK_O
  , output CL2_IN_V_O
  , input CL2_IN_TKN_I
  , output CL2_IN_D0_O
  , output CL2_IN_D1_O
  , output CL2_IN_D2_O
  , output CL2_IN_D3_O
  , output CL2_IN_D4_O
  , output CL2_IN_D5_O
  , output CL2_IN_D6_O
  , output CL2_IN_D7_O
  , output CL2_IN_D8_O

  , output CL2_OUT_CLK_O
  , output CL2_OUT_V_O
  , input CL2_OUT_TKN_I
  , output CL2_OUT_D0_O
  , output CL2_OUT_D1_O
  , output CL2_OUT_D2_O
  , output CL2_OUT_D3_O
  , output CL2_OUT_D4_O
  , output CL2_OUT_D5_O
  , output CL2_OUT_D6_O
  , output CL2_OUT_D7_O
  , output CL2_OUT_D8_O

  , output TAG_CLK_O
  , output TAG_EN_O
  , output TAG_DATA_O

  , output CLKA_O
  , output CLKB_O
  , output CLKC_O

  , input CLK0_I
  
  , output SEL0_O
  , output SEL1_O
  , output SEL2_O

  , output CLK_RESET_O
  , output CORE_RESET_O
);

bsg_gateway_chip bgc
(
  .p_ci_clk_i     (CL_IN_CLK_I)
  ,.p_ci_v_i      (CL_IN_V_I)
  ,.p_ci_tkn_o    (CL_IN_TKN_O)
  ,.p_ci_0_i      (CL_IN_D0_I)
  ,.p_ci_1_i      (CL_IN_D1_I)
  ,.p_ci_2_i      (CL_IN_D2_I)
  ,.p_ci_3_i      (CL_IN_D3_I)
  ,.p_ci_4_i      (CL_IN_D4_I)
  ,.p_ci_5_i      (CL_IN_D5_I)
  ,.p_ci_6_i      (CL_IN_D6_I)
  ,.p_ci_7_i      (CL_IN_D7_I)
  ,.p_ci_8_i      (CL_IN_D8_I)

  ,.p_co_clk_i    (CL_OUT_CLK_I)
  ,.p_co_v_i      (CL_OUT_V_I)
  ,.p_co_tkn_o    (CL_OUT_TKN_O)
  ,.p_co_0_i      (CL_OUT_D0_I)
  ,.p_co_1_i      (CL_OUT_D1_I)
  ,.p_co_2_i      (CL_OUT_D2_I)
  ,.p_co_3_i      (CL_OUT_D3_I)
  ,.p_co_4_i      (CL_OUT_D4_I)
  ,.p_co_5_i      (CL_OUT_D5_I)
  ,.p_co_6_i      (CL_OUT_D6_I)
  ,.p_co_7_i      (CL_OUT_D7_I)
  ,.p_co_8_i      (CL_OUT_D8_I)

  ,.p_ci2_clk_o   (CL2_IN_CLK_O)
  ,.p_ci2_v_o     (CL2_IN_V_O)
  ,.p_ci2_tkn_i   (CL2_IN_TKN_I)
  ,.p_ci2_0_o     (CL2_IN_D0_O)
  ,.p_ci2_1_o     (CL2_IN_D1_O)
  ,.p_ci2_2_o     (CL2_IN_D2_O)
  ,.p_ci2_3_o     (CL2_IN_D3_O)
  ,.p_ci2_4_o     (CL2_IN_D4_O)
  ,.p_ci2_5_o     (CL2_IN_D5_O)
  ,.p_ci2_6_o     (CL2_IN_D6_O)
  ,.p_ci2_7_o     (CL2_IN_D7_O)
  ,.p_ci2_8_o     (CL2_IN_D8_O)

  ,.p_co2_clk_o   (CL2_OUT_CLK_O)
  ,.p_co2_v_o     (CL2_OUT_V_O)
  ,.p_co2_tkn_i   (CL2_OUT_TKN_I)
  ,.p_co2_0_o     (CL2_OUT_D0_O)  
  ,.p_co2_1_o     (CL2_OUT_D1_O)
  ,.p_co2_2_o     (CL2_OUT_D2_O)
  ,.p_co2_3_o     (CL2_OUT_D3_O)
  ,.p_co2_4_o     (CL2_OUT_D4_O)
  ,.p_co2_5_o     (CL2_OUT_D5_O)
  ,.p_co2_6_o     (CL2_OUT_D6_O)
  ,.p_co2_7_o     (CL2_OUT_D7_O)
  ,.p_co2_8_o     (CL2_OUT_D8_O)

  ,.p_bsg_tag_clk_o   (TAG_CLK_O)
  ,.p_bsg_tag_en_o    (TAG_EN_O)
  ,.p_bsg_tag_data_o  (TAG_DATA_O)
  ,.p_bsg_tag_clk_i   (1'b0)
  ,.p_bsg_tag_data_i  (1'b0)

  ,.p_clk_A_o     (CLKA_O)
  ,.p_clk_B_o     (CLKB_O)
  ,.p_clk_C_o     (CLKC_O)

  ,.p_clk_i       (CLK0_I)

  ,.p_sel_0_o     (SEL0_O)
  ,.p_sel_1_o     (SEL1_O)
  ,.p_sel_2_o     (SEL2_O)

  ,.p_clk_async_reset_o   (CLK_RESET_O)
  ,.p_core_async_reset_o  (CORE_RESET_O)

  ,.p_misc_i      (1'b0)
);

endmodule
