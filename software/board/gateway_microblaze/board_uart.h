#ifndef BOARD_UART_H		/* prevent circular inclusions */
#define BOARD_UART_H		/* by using protection macros */

#include "xil_types.h"
#include "xil_assert.h"
#include "xstatus.h"

int uart_init();
int uart_send(const u8* buffer, int len);
int uart_recv(u8* buffer, int len);

#endif

