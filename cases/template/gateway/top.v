//-----------------------------------------------------------------------------
// board_ctrl_top.v
//-----------------------------------------------------------------------------

module top
(
    PWR_RSTN,
    CLK_OSC_P,
    CLK_OSC_N,
    ASIC_CORE_EN,
    ASIC_IO_EN,
    CUR_MON_ADDR0,
    CUR_MON_ADDR1,
    CUR_MON_SCL,
    CUR_MON_SDA,
    DIG_POT_ADDR0,
    DIG_POT_ADDR1,
    DIG_POT_INDEP,
    DIG_POT_NRST,
    DIG_POT_SCL,
    DIG_POT_SDA,
    UART_RX,
    UART_TX,
    FPGA_LED0,
    FPGA_LED1,
    FPGA_LED2,
    FPGA_LED3,
    FG_SW0,
    FG_SW1,
    FG_SW2,
    FG_SW3,
    FG_SW4,
    FG_SW5,
    FG_SW6,
    FG_SW7
);

input PWR_RSTN;
input CLK_OSC_P;
input CLK_OSC_N;
output reg ASIC_CORE_EN;
output reg ASIC_IO_EN;
output reg CUR_MON_ADDR0;
output reg CUR_MON_ADDR1;
inout CUR_MON_SCL;
inout CUR_MON_SDA;
output reg DIG_POT_ADDR0;
output reg DIG_POT_ADDR1;
output reg DIG_POT_INDEP;
output reg DIG_POT_NRST;
inout DIG_POT_SCL;
inout DIG_POT_SDA;
input UART_RX;
output UART_TX;
output reg FPGA_LED0;
output reg FPGA_LED1;
output FPGA_LED2;
output FPGA_LED3;
input FG_SW0;
input FG_SW1;
input FG_SW2;
input FG_SW3;
input FG_SW4;
input FG_SW5;
input FG_SW6;
input FG_SW7;

assign FPGA_LED2 = ASIC_IO_EN;
assign FPGA_LED3 = ASIC_CORE_EN;

wire [11:0] gpio_1;
wire cpu_override_output_p;
wire cpu_override_output_n;
assign cpu_override_output_p = gpio_1[11];
assign cpu_override_output_n = gpio_1[10];
always @(*)
begin
    {ASIC_CORE_EN, ASIC_IO_EN, CUR_MON_ADDR1, CUR_MON_ADDR0, DIG_POT_ADDR1, DIG_POT_ADDR0, DIG_POT_NRST, DIG_POT_INDEP, FPGA_LED1, FPGA_LED0} = 10'b1111111111;
    if (cpu_override_output_p == 1'b1 && cpu_override_output_n == 1'b0 && PWR_RSTN == 1'b1) begin
	     {ASIC_CORE_EN, ASIC_IO_EN, CUR_MON_ADDR1, CUR_MON_ADDR0, DIG_POT_ADDR1, DIG_POT_ADDR0, DIG_POT_NRST, DIG_POT_INDEP, FPGA_LED1, FPGA_LED0} = gpio_1[9:0];
    end
end

wire [7:0] gpio_2;
assign gpio_2 = {FG_SW7, FG_SW6, FG_SW5, FG_SW4, FG_SW3, FG_SW2, FG_SW1, FG_SW0};

(* BOX_TYPE = "user_black_box" *)
board_ctrl board_ctrl_i (
	.RESET ( PWR_RSTN ),
	.CLK_P ( CLK_OSC_P ),
	.CLK_N ( CLK_OSC_N ),
	.axi_iic_dig_pot_Gpo_pin (  ),
	.axi_iic_dig_pot_Sda_pin ( DIG_POT_SDA ),
	.axi_iic_dig_pot_Scl_pin ( DIG_POT_SCL ),
	.axi_iic_cur_mon_Gpo_pin (  ),
	.axi_iic_cur_mon_Sda_pin ( CUR_MON_SDA ),
	.axi_iic_cur_mon_Scl_pin ( CUR_MON_SCL ),
	.axi_gpio_0_GPIO_IO_O_pin ( gpio_1 ),
	.axi_gpio_0_GPIO2_IO_I_pin ( gpio_2 ),
	.axi_uartlite_0_RX_pin ( UART_RX ),
	.axi_uartlite_0_TX_pin ( UART_TX )
 );

endmodule
