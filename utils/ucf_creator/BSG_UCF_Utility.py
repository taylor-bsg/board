#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Copyright (C) 2014-2015 UCSD Bespoken Systems Group
# All rights reserved
# Created by Shengye Wang <shengye@ucsd.edu>

# ----------------------------------- Please modify below -----------------------------------
conn_filename = "DaughterBoardsSchExport_Connection.txt"
interest_prefix = {'U2-':'ASIC_Emulator', 'U4-':'Gateway_FPGA'}
part_map = {'U2-':'Part_Info/XC6SLX150-FGG484.txt', 'U4-':'Part_Info/XC6SLX150-FGG676.txt'}
filter_list = ['P1V2', 'P2V5', 'P3V3', 'GND', 'PADJ_ASIC_IO', 'PADJ_ASIC_CO']
board_name = "UCSD BSG Double Trouble V1"
# ----------------------------------- Please modify above -----------------------------------

import time

def print_columns(lines, spacing = 2):
    widths = [max(len(value) for value in column) + spacing for column in zip(*lines)]
    rtn = ''
    for line in lines:
        rtn += ("%s\n" % (''.join('%-*s' % item for item in zip(widths, line)).strip()))
    return rtn

# Read connection file for parsing
fp_input = open(conn_filename)
lines = fp_input.readlines()
fp_input.close()

final_dict = {} # Parse result: index is net name, value is array of part pinouts

# Parse connection file
sig_name = ""
for line in lines:
    splited = line.split()
    if (len(splited) == 4 and splited[0] == '*SIGNAL*'):
        sig_name = splited[1]
    elif sig_name != "":
        if (len(splited) == 0):
            sig_name == ""
            continue
        if (len(splited) == 4):
            if (not sig_name in final_dict):
                final_dict[sig_name] = []
            for anno in [splited[0], splited[1]]:
                if not anno in final_dict[sig_name]:
                    final_dict[sig_name].append(anno)

# Generate ucf
for pfx in interest_prefix:
    pin_description = {}
    fp_desc_input = open(part_map[pfx], 'r')
    desc_lines = fp_desc_input.readlines()
    fp_desc_input.close()
    for line in desc_lines:
        splited = line.split()
        if (len(splited) == 2):
            pin_description[splited[0]] = splited[1]
    outlist = []
    for sig_name in final_dict:
        if sig_name in filter_list:
            continue
        for anno in final_dict[sig_name]:
            if (anno.find(pfx) == 0):
                pin_loc = anno[anno.find('.') + 1:]
                if (pin_loc in pin_description):
                    outlist.append(('NET "%s"' % sig_name, 'LOC = "%s"' % pin_loc, '# %s' % pin_description[pin_loc]))
                else:
                    outlist.append(('NET "%s"' % sig_name, 'LOC = "%s"' % pin_loc, ''))
    outlist.sort()
    fp_output = open(("%s.ucf" % interest_prefix[pfx]), 'w')
    fp_output.write("# %s\n" % board_name)
    fp_output.write("# Xilinx UCF %s\n" % interest_prefix[pfx])
    fp_output.write("# Created %s \n\n" % time.asctime())
    fp_output.write(print_columns(outlist))
    fp_output.close()

